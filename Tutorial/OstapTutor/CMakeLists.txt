################################################################################
# Package: OstapTutor
################################################################################
gaudi_subdir(OstapTutor)

gaudi_depends_on_subdirs(Analysis/Ostap)

gaudi_install_python_modules()

