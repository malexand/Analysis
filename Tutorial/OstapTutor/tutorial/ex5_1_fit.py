#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ============================================================================
# $Id$
# ============================================================================
## @file ex5_1_fit.py
#  Use bare RooAbsPDF via Generic1D 
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2014-12-10
#
#                    $Revision$
#  Last modification $Date$
#                 by $Author$
# ============================================================================
""" Example of simple fit:
use bare RooAbsPdf via Generic1D 
"""
# ============================================================================
__version__ = "$Revision$"
__author__  = "Vanya BELYAEV  Ivan.Belyaev@itep.ru"
__date__    = "2014-12-10"
# ============================================================================
import ROOT, random
from   Ostap.PyRoUts  import *
# ============================================================================
# logging
# ============================================================================
from AnalysisPython.Logger import getLogger
if __name__  in ( '__main__' , '__builtin__' ) : logger = getLogger( 'ex5_fit')
else : logger = getLogger( __name__ )
logger.info ('Simple fit')
# ============================================================================

## 1) import dataset and variable 
from OstapTutor.TestData2 import m_psi, data

logger.info ( 'Data: %s' % data ) 

## 2) create the model: signal + background


## 2a) create the  signal: 

sigma = ROOT.RooRealVar('sigma','sigma', 0.005 ,  0.020 )
mean  = ROOT.RooRealVar('mean' ,'mean' , 3.050 ,  3.150 )
sigma.setVal(0.013)
mean .setVal(3.096)

gaussian = ROOT.RooGaussian('Gauss','Gaussian', m_psi , mean , sigma )

## 2b) create the background:
c0        = ROOT.RooRealVar('c0','coefficient #0', 1.0,-1.,1.)
c1        = ROOT.RooRealVar('c1','coefficient #1', 0. ,-1.,1.)
c2        = ROOT.RooRealVar('c2','coefficient #2', 0  ,-1.,1.)
chebyshev = ROOT.RooChebychev('bkg','background p.d.f.',m_psi,ROOT.RooArgList(c0,c1,c2))

## 3) wrap them into Ostap
import Ostap.FitModels as Models

signal      = Models.Generic1D_pdf( gaussian  , m_psi , name = 'Sig' )
background  = Models.Generic1D_pdf( chebyshev , m_psi , name = 'Bkg' )


##  4) create the model

model = Models.Fit1D ( signal     = signal     ,
                       background = background )  

## 3) try to fit:

r,f = model.fitTo  ( data , silence = True , ncpu = 8 ) 

## 4) try to fit and draw it:

r,f = model.fitTo  ( data , ncpu = 8 , draw = True , silence = True ) 




# ============================================================================
# The END 
# ============================================================================
