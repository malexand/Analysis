2017-07-21 Analysis v19r0
=========================

Release for 2017 production
---------------------------

This version is released on the master branch.
It is based on Gaudi v28r2, LHCb v43r0, Lbcom v21r0, Rec v22r0 and Phys v24r0
and uses LCG_88 with ROOT 6.08.06.

- Add Tuple tool TupleToolMCAssociatedClusters to investigate the associated clusters
  - See merge request !134
- Ostap : improvements to Data.py and TreeDeco.py
  - See merge request !133
