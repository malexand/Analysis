#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
## @file Test3DModels.py
#
#  tests for various 3D-smooth non-factorizable models
#
#  @author Vanya BELYAEV Ivan.Belyaeve@itep.ru
#  @date 2017-11-22
# =============================================================================
"""
Tests for various 3D-smooth non-factorizable models
"""
# =============================================================================
__version__ = "$Revision:"
__author__  = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__    = "2017-11-22"
__all__     = ()  ## nothing to be imported 
# =============================================================================
import ROOT, random 
from   Ostap.PyRoUts import *
from   Ostap.Utils   import rooSilent 
# =============================================================================
# logging 
# =============================================================================
from AnalysisPython.Logger import getLogger
if '__main__' == __name__  or '__builtin__'  == __name__ :  
    logger = getLogger( 'Ostap.Test3DModels' )
else : 
    logger = getLogger( __name__ )
# =============================================================================
logger.info ( 'Test for 3D nonfactorizable fit models from Analysis/Ostap')
# =============================================================================
## make simple test mass 
m_x     = ROOT.RooRealVar ( 'mass_x' , 'Some test mass(X)' , 3 , 3.2 )
m_y     = ROOT.RooRealVar ( 'mass_y' , 'Some test mass(Y)' , 3 , 3.2 )
m_z     = ROOT.RooRealVar ( 'mass_z' , 'Some test mass(Z)' , 3 , 3.2 )

## book very simple data set
varset  = ROOT.RooArgSet  ( m_x , m_y , m_z )
dataset = ROOT.RooDataSet ( dsID() , 'Test Data set-1' , varset )  

## fill it with uniform (x,y) 
m = VE(3.100,0.015**2)
w = VE(3.100,0.100**2) 
for i in xrange(0,100000) :

    m_x.value = random.uniform ( *m_x.minmax() ) 
    m_y.value = random.uniform ( *m_y.minmax() ) 
    m_z.value = random.uniform ( *m_z.minmax() ) 
    
    dataset.add ( varset  )

print dataset

import Ostap.Fit3DModels as Models 

models = []

# =============================================================================
## Positive polynomial in X,Y,Z  
# =============================================================================
logger.info ('Test PolyPos3D_pdf' )
poly3D = Models.PolyPos3D_pdf ( 'P3DP',
                                m_x  ,
                                m_y  ,
                                m_z  ,
                                nx = 1 , ny = 1 , nz = 1  )

with rooSilent() : 
    result = poly3D.fitTo ( dataset ) 
    result = poly3D.fitTo ( dataset ) 
    result = poly3D.fitTo ( dataset ) 
    
poly3D.pdf.setPars() 
pos  = poly3D.pdf.function()
nsph = pos.sphere()  
b3d  = pos.bernstein()

if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual () ) )
    print result 
else :
    print 'Bernstein Coefficients:' , b3d.pars()
    print  result
    
models.append ( poly3D )


# =============================================================================
## Positive SYMMETRIC polynomial in X,Y,Z  
# =============================================================================
logger.info ('Test PolyPos3Dsym_pdf' )
poly3Ds = Models.PolyPos3Dsym_pdf ( 'P3DS' ,
                                   m_x    ,
                                   m_y    ,
                                   m_z    , 
                                   n  = 1 )

with rooSilent() : 
    result = poly3Ds.fitTo ( dataset ) 
    result = poly3Ds.fitTo ( dataset ) 
    result = poly3Ds.fitTo ( dataset ) 
    
poly3Ds.pdf.setPars() 
pos  = poly3Ds.pdf.function()
nsph = pos.sphere()  
b3d  = pos.bernstein()

if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual () ) )
    print result
else :
    print 'Bernstein Coefficients:' , b3d.pars()
    print  result
    
models.append ( poly3Ds )

# =============================================================================
## Positive partial SYMMETRIC ( x<-->y ) polynomial in X,Y,Z  
# =============================================================================
logger.info ('Test PolyPos3Dmix_pdf' )
poly3Dm = Models.PolyPos3Dmix_pdf ( 'P3DM'  ,
                                    m_x     ,
                                    m_y     ,
                                    m_z     , 
                                    n  = 1  , nz = 1 )

with rooSilent() : 
    result = poly3Dm.fitTo ( dataset ) 
    result = poly3Dm.fitTo ( dataset ) 
    result = poly3Dm.fitTo ( dataset ) 
    result = poly3Dm.fitTo ( dataset ) 
    result = poly3Dm.fitTo ( dataset ) 
    
poly3Dm.pdf.setPars() 
pos  = poly3Dm.pdf.function()
nsph = pos.sphere()  
b3d  = pos.bernstein()

if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual () ) )
    print result 
else :
    print 'Bernstein Coefficients:' , b3d.pars()
    print  result
    
models.append ( poly3Dm )


#
## check that everything is serializeable
# 
import Ostap.ZipShelve   as DBASE
with DBASE.tmpdb() as db :
    db['x,y,vars'] = m_x, m_y,m_z, varset
    db['dataset' ] = dataset
    db['models'  ] = models
    db['result'  ] = result
    db.ls()
    

# =============================================================================
# The END 
# =============================================================================
