#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
## @file TestComponents.py
#
#  tests for various multicomponents models 
#
#  @author Vanya BELYAEV Ivan.Belyaeve@itep.ru
#  @date 2014-05-11
# =============================================================================
"""Tests for various multicomponent models 
"""
# =============================================================================
__version__ = "$Revision:"
__author__  = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__    = "2014-05-10"
__all__     = ()  ## nothing to be imported 
# =============================================================================
import ROOT, random
from   Ostap.PyRoUts import *
from   Ostap.Utils   import rooSilent 
# =============================================================================
# logging 
# =============================================================================
from AnalysisPython.Logger import getLogger
if '__main__' == __name__  or '__builtin__' == __name__ : 
    logger = getLogger ( 'Ostap.TestComponents' )
else : 
    logger = getLogger ( __name__ )
# =============================================================================
logger.info ( 'Test for multi-component models from Analysis/Ostap')
# =============================================================================
## make simple test mass 
mass    = ROOT.RooRealVar ( 'test_mass' , 'Some test mass' , 0 , 10 )

## book very simple data set
varset  = ROOT.RooArgSet  ( mass )
dataset = ROOT.RooDataSet ( dsID() , 'Test Data set-0' , varset )  

mmin, mmax = mass.minmax() 

### fill it 
m1 = VE(3,0.300**2)
m2 = VE(5,0.200**2)
m3 = VE(7,0.100**2)

for i in xrange(0,5000) :
    for m in (m1,m2,m3) : 
        mass.value = m.gauss () 
        dataset.add ( varset    )

for i in xrange(0,5000) :
    mass.value = random.uniform ( *mass.minmax() ) 
    dataset.add ( varset   )

logger.info ('Dataset: %s' % dataset )  
import Ostap.FitModels as Models

signal_1 = Models.Gauss_pdf ( 'G1'  , xvar = mass , mean = m1.value() , sigma = m1.error() )
signal_2 = Models.Gauss_pdf ( 'G2'  , xvar = mass , mean = m2.value() , sigma = m2.error() )
signal_3 = Models.Gauss_pdf ( 'G3'  , xvar = mass , mean = m3.value() , sigma = m3.error() )

wide_1   = Models.Gauss_pdf ( 'GW1' , xvar = mass , mean = 1.0 , sigma = 2 )
wide_2   = Models.Gauss_pdf ( 'GW2' , xvar = mass , mean = 9.0 , sigma = 3 )

narrow_1 = Models.Gauss_pdf ( 'GN1' , xvar = mass , mean = 4.0 , sigma = 1 )
narrow_2 = Models.Gauss_pdf ( 'GN2' , xvar = mass , mean = 6.0 , sigma = 1 )

## ============================================================================
logger.info ( 'Test the extended fit with many components' ) 
model_ext1 = Models.Fit1D (
    name             = 'EXT1'     , 
    signal           =   signal_1 ,
    othersignals     = [ signal_2 , signal_3 ] ,
    background       =   wide_1   ,
    otherbackgrounds = [ wide_2 ] ,
    others           = [ narrow_1 , narrow_2 ] ,
    )
model_ext1.S[0].value = 5000
model_ext1.S[1].value = 5000 
model_ext1.S[2].value = 5000 

model_ext1.B[0].value = 1700
model_ext1.B[1].value = 2300 

model_ext1.C[0].value =  500
model_ext1.C[1].value =  400 

r, f = model_ext1.fitTo ( dataset , draw = False , silent = True )
r, f = model_ext1.fitTo ( dataset , draw = False , silent = True )
logger.info ( 'Model %s Fit results:\n#%s ' % ( model_ext1.name , r ) ) 
print 'Signals               [S]:' , model_ext1.S
print 'Backgrounds           [B]:' , model_ext1.B
print 'Components            [C]:' , model_ext1.C
print 'Fractions             [F]:' , model_ext1.F
print 'Signal fractions     [fS]:' , model_ext1.fS
print 'Background fractions [fB]:' , model_ext1.fB
print 'Component fractions  [fC]:' , model_ext1.fC
print 'Yields           [yields]:' , model_ext1.yields
print 'Fractions     [fractions]:' , model_ext1.fractions


## ============================================================================
logger.info ( 'Test the extended fit with compound components' ) 
model_ext2 = Models.Fit1D (
    name                = 'EXT2'     , 
    signal              =   signal_1 ,
    othersignals        = [ signal_2 , signal_3 ] ,
    background          =   wide_1   ,
    otherbackgrounds    = [ wide_2 ] ,
    others              = [ narrow_1 , narrow_2 ] ,
    #
    combine_signals     = True   ,
    combine_backgrounds = True   ,
    combine_others      = True   ,    
    )
model_ext2.S  = 5000
model_ext2.B  = 4200
model_ext2.C  =  700

model_ext2.fS[0].value = 0.33 
model_ext2.fS[1].value = 0.50

model_ext2.fB[0].value = 0.40 
model_ext2.fC[0].value = 0.60 

r, f = model_ext2.fitTo ( dataset , draw = False , silent = True )
r, f = model_ext2.fitTo ( dataset , draw = False , silent = True )
logger.info ( 'Model %s Fit results:\n#%s ' % ( model_ext2.name , r ) ) 
print 'Signals               [S]:' , model_ext2.S
print 'Backgrounds           [B]:' , model_ext2.B
print 'Components            [C]:' , model_ext2.C
print 'Fractions             [F]:' , model_ext2.F
print 'Signal fractions     [fS]:' , model_ext2.fS
print 'Background fractions [fB]:' , model_ext2.fB
print 'Component fractions  [fC]:' , model_ext2.fC
print 'Yields           [yields]:' , model_ext2.yields
print 'Fractions     [fractions]:' , model_ext2.fractions

## ============================================================================
logger.info ( 'Test non-extended fit with all components, non-recursive' ) 
model_ne1 = Models.Fit1D (
    name                = 'NE1'                   , 
    signal              =   signal_1              , 
    othersignals        = [ signal_2 , signal_3 ] ,
    background          =   wide_1   ,
    otherbackgrounds    = [ wide_2 ] ,
    others              = [ narrow_1 , narrow_2 ] ,
    ##
    extended            = False , 
    recursive           = False 
    )

model_ne1.F[0].value = 0.25
model_ne1.F[1].value = 0.25
model_ne1.F[2].value = 0.25
model_ne1.F[3].value = 0.08
model_ne1.F[4].value = 0.12
model_ne1.F[5].value = 0.05

r, f = model_ne1.fitTo ( dataset , draw = False , silent = True )
r, f = model_ne1.fitTo ( dataset , draw = False , silent = True )
logger.info ( 'Model %s Fit results:\n#%s ' % ( model_ne1.name , r ) ) 
print 'Signals               [S]:' , model_ne1.S
print 'Backgrounds           [B]:' , model_ne1.B
print 'Components            [C]:' , model_ne1.C
print 'Fractions             [F]:' , model_ne1.F
print 'Signal fractions     [fS]:' , model_ne1.fS
print 'Background fractions [fB]:' , model_ne1.fB
print 'Component fractions  [fC]:' , model_ne1.fC
print 'Yields           [yields]:' , model_ne1.yields
print 'Fractions     [fractions]:' , model_ne1.fractions

## ============================================================================
logger.info ( 'Test non-extended fit with all components, recursive' ) 
model_ne2 = Models.Fit1D (
    name                = 'NE2'                   , 
    signal              =   signal_1              , 
    othersignals        = [ signal_2 , signal_3 ] ,
    background          =   wide_1   ,
    otherbackgrounds    = [ wide_2 ] ,
    others              = [ narrow_1 , narrow_2 ] , 
    ##
    extended            = False , 
    recursive           = True  ,
    )

model_ne2.F[0].value = 0.25
model_ne2.F[1].value = 0.33
model_ne2.F[2].value = 0.50
model_ne2.F[3].value = 0.37
model_ne2.F[4].value = 0.74
model_ne2.F[5].value = 0.50

r, f = model_ne2.fitTo ( dataset , draw = False , silent = True )
r, f = model_ne2.fitTo ( dataset , draw = False , silent = True )
logger.info ( 'Model %s Fit results:\n#%s ' % ( model_ne2.name , r ) ) 
print 'Signals               [S]:' , model_ne2.S
print 'Backgrounds           [B]:' , model_ne2.B
print 'Components            [C]:' , model_ne2.C
print 'Fractions             [F]:' , model_ne2.F
print 'Signal fractions     [fS]:' , model_ne2.fS
print 'Background fractions [fB]:' , model_ne2.fB
print 'Component fractions  [fC]:' , model_ne2.fC
print 'Yields           [yields]:' , model_ne2.yields
print 'Fractions     [fractions]:' , model_ne2.fractions


## ============================================================================
logger.info ( 'Test non-extended fit with compound components, non-recursive' ) 
model_ne3 = Models.Fit1D (
    name                = 'NE2'                   , 
    signal              =   signal_1              , 
    othersignals        = [ signal_2 , signal_3 ] ,
    background          =   wide_1   ,
    otherbackgrounds    = [ wide_2 ] ,
    others              = [ narrow_1 , narrow_2 ] , 
    ##
    combine_signals     = True   ,
    combine_backgrounds = True   ,
    combine_others      = True   ,
    ##
    extended            = False  ,
    recursive           = False 
    )

model_ne3.F[0].value  = 0.75
model_ne3.F[1].value  = 0.30

model_ne3.fS[0].value = 0.33
model_ne3.fS[1].value = 0.50

model_ne3.fB[0].value = 0.41
model_ne3.fC[0].value = 0.58

r, f = model_ne3.fitTo ( dataset , draw = False , silent = True )
r, f = model_ne3.fitTo ( dataset , draw = False , silent = True )
logger.info ( 'Model %s Fit results:\n#%s ' % ( model_ne3.name , r ) ) 
print 'Signals               [S]:' , model_ne3.S
print 'Backgrounds           [B]:' , model_ne3.B
print 'Components            [C]:' , model_ne3.C
print 'Fractions             [F]:' , model_ne3.F
print 'Signal fractions     [fS]:' , model_ne3.fS
print 'Background fractions [fB]:' , model_ne3.fB
print 'Component fractions  [fC]:' , model_ne3.fC
print 'Yields           [yields]:' , model_ne3.yields
print 'Fractions     [fractions]:' , model_ne3.fractions


## ============================================================================
logger.info ( 'Test non-extended fit with compound components, recursive' ) 
model_ne4 = Models.Fit1D (
    name                = 'NE4'                   , 
    signal              =   signal_1              , 
    othersignals        = [ signal_2 , signal_3 ] ,
    background          =   wide_1   ,
    otherbackgrounds    = [ wide_2 ] ,
    others              = [ narrow_1 , narrow_2 ] , 
    ##
    combine_signals     = True   ,
    combine_backgrounds = True   ,
    combine_others      = True   ,
    ##
    extended            = False  , 
    recursive           = True    
    )

model_ne4.F[0].value  = 0.75
model_ne4.F[1].value  = 0.80

model_ne4.fS[0].value = 0.33
model_ne4.fS[1].value = 0.50

model_ne4.fB[0].value = 0.41
model_ne4.fC[0].value = 0.50

r, f = model_ne4.fitTo ( dataset , draw = False , silent = True )
r, f = model_ne4.fitTo ( dataset , draw = False , silent = True )
logger.info ( 'Model %s Fit results:\n#%s ' % ( model_ne4.name , r ) ) 
print 'Signals               [S]:' , model_ne4.S
print 'Backgrounds           [B]:' , model_ne4.B
print 'Components            [C]:' , model_ne4.C
print 'Fractions             [F]:' , model_ne4.F
print 'Signal fractions     [fS]:' , model_ne4.fS
print 'Background fractions [fB]:' , model_ne4.fB
print 'Component fractions  [fC]:' , model_ne4.fC
print 'Yields           [yields]:' , model_ne4.yields
print 'Fractions     [fractions]:' , model_ne4.fractions


# =============================================================================
# The END 
# =============================================================================
