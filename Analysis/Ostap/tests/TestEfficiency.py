#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# Copyright (c) Ostap developers.
# ============================================================================= 
# @file TestEfficiency.py
# Test module for Ostap/RitEfficiency.py
# ============================================================================= 
""" Test module for Ostap/RitEfficiency.py
"""
# ============================================================================= 
__author__ = "Ostap developers"
__all__    = () ## nothing to import
# ============================================================================= 
import ROOT, random
import Ostap.RooFitDeco 
import Ostap.FitModels as     Models 
from   Ostap.PyRoUts   import cpp, VE, dsID
# =============================================================================
# logging 
# =============================================================================
from Ostap.Logger import getLogger
if '__main__' == __name__  or '__builtin__' == __name__ : 
    logger = getLogger ( 'test_efficiency' )
else : 
    logger = getLogger ( __name__ )
# =============================================================================
## make
x   = ROOT.RooRealVar ( 'x',  'test' , 0 , 1 )
acc = ROOT.RooCategory( 'cut','cut')
acc.defineType('accept',1)
acc.defineType('reject',0)
varset  = ROOT.RooArgSet ( x , acc )
ds      = ROOT.RooDataSet( dsID() , 'test data' ,  varset )

for i in range ( 10000 ) :
    v = random.gauss ( 3 , 2 ) / 10 
    ## v = random.uniform ( 0 , 1  ) 
    if not 0  <  v < 1 : continue 
    x.setVal ( v ) 
    acc.setIndex(0)
    ds.add ( varset )
    
for i in range ( 10000 ) :
    v = random.gauss ( 7 , 2 ) / 10
    ## v = random.uniform ( 0 , 1  ) 
    if not  0  <  v < 1 : continue
    x.setVal ( v ) 
    acc.setIndex(1)
    ds.add ( varset )

from Ostap.FitEfficiency import Efficiency1D

points =  [ 0.1 * i for i in range (10) ]
# =============================================================================
# use RooFormulaVar to parameterise efficiency:
def test_formula () :
    
    a       = ROOT.RooRealVar('a','a',0.0001,1.e-6,0.95)
    b       = ROOT.RooRealVar('b','b',  1,0,2)
    c       = ROOT.RooRealVar('c','c',  1,0.01,10)
    effFunc = ROOT.RooFormulaVar ("effFunc","(1-a)+a*cos((x-b)/c)",ROOT.RooArgList(x,a,b,c))
        
    eff1 = Efficiency1D( 'E1' , effFunc , cut  = acc , xvar = x )
    r1 = eff1.fitTo ( ds )
    f1 = eff1.draw  ( ds )
    print r1
    for p in points :
        eff =  eff1 ( p , error = True ) * 100 
        print ' Point/Eff %.1f %s%%'   % ( p , eff.toString ( '(%5.2f+-%4.2f)' ) )
        
    
# =============================================================================
# use some PDF to parameterize efficiciency
def test_pdf () : 
    effPdf = Models.PolyPos_pdf ( 'B' , xvar = x , power = 6 )
    
    eff2 = Efficiency1D( 'E2' , effPdf , cut = acc  )
    r2 = eff2.fitTo ( ds )
    f2 = eff2.draw  (  ds )
    print r2
    for p in points :
        eff =  eff2 ( p , error = True ) * 100 
        print ' Point/Eff %.1f %s%%'   % ( p , eff.toString ( '(%5.2f+-%4.2f)' ) )

    
# =============================================================================
if '__main__' == __name__ :
    
    test_formula ()
    test_pdf     ()
    

# =============================================================================
# The END 
# ============================================================================= 
