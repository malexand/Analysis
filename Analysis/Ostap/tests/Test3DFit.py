#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
## @file Test3DFit.py
#
#  Tests for 3D-fit machinery
#
#  @author Vanya BELYAEV Ivan.Belyaeve@itep.ru
#  @date 2017-11-22
# =============================================================================
"""
Tests for 3D-fit machinery
"""
# =============================================================================
__version__ = "$Revision:"
__author__  = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__    = "2017-11-22"
__all__     = ()  ## nothing to be imported 
# =============================================================================
import ROOT, random 
from   Ostap.PyRoUts import *
from   Ostap.Utils   import rooSilent 
# =============================================================================
# logging 
# =============================================================================
from AnalysisPython.Logger import getLogger
if '__main__' == __name__  or '__builtin__'  == __name__ :  
    logger = getLogger( 'Ostap.Test3DFit' )
else : 
    logger = getLogger( __name__ )
# =============================================================================
logger.info ( 'Test for 3D nonfactorizable fit models from Analysis/Ostap')
# =============================================================================
## make simple test mass 
m_x     = ROOT.RooRealVar ( 'mass_x' , 'Some test mass(X)' , 3 , 3.2 )
m_y     = ROOT.RooRealVar ( 'mass_y' , 'Some test mass(Y)' , 3 , 3.2 )
m_z     = ROOT.RooRealVar ( 'mass_z' , 'Some test mass(Z)' , 3 , 3.2 )

## book very simple data set
varset  = ROOT.RooArgSet  ( m_x , m_y , m_z )
dataset = ROOT.RooDataSet ( dsID() , 'Test Data set-1' , varset )  


## SxSxS
for i in xrange(0,10000) :

    x = random.gauss(3.1,0.015)
    y = random.gauss(3.1,0.015)
    z = random.gauss(3.1,0.015)

    m_x.setVal ( x )
    m_y.setVal ( y )
    m_z.setVal ( z )
    
    dataset.add ( varset  )


## SxSxB
for i in xrange(0,5000) :

    x = random.gauss(3.1,0.015)
    y = random.gauss(3.1,0.015)
    z = random.uniform (  *m_z.minmax() )

    m_x.setVal ( x )
    m_y.setVal ( y )
    m_z.setVal ( z )

    dataset.add ( varset  )

    x = random.gauss(3.1,0.015)
    y = random.uniform ( *m_y.minmax() )
    z = random.gauss(3.1,0.015)
    
    m_x.setVal ( x )
    m_y.setVal ( y )
    m_z.setVal ( z )
    
    dataset.add ( varset  )

    x = random.uniform ( *m_x.minmax() )
    y = random.gauss(3.1,0.015)
    z = random.gauss(3.1,0.015)
    
    m_x.setVal ( x )
    m_y.setVal ( y )
    m_z.setVal ( z )
    
    dataset.add ( varset  )

## SxBxB
for i in xrange(0,8000) :

    x = random.gauss   (3.1,0.015)
    y = random.uniform ( *m_y.minmax() )
    z = random.uniform ( *m_z.minmax() )

    m_x.setVal ( x )
    m_y.setVal ( y )
    m_z.setVal ( z )

    dataset.add ( varset  )

    x = random.uniform ( *m_x.minmax() )
    y = random.gauss   (3.1,0.015)
    z = random.uniform (  *m_z.minmax() )

    m_x.setVal ( x )
    m_y.setVal ( y )
    m_z.setVal ( z )
    
    dataset.add ( varset  )
    
    x = random.uniform ( *m_x.minmax() )
    y = random.uniform ( *m_y.minmax() )
    z = random.gauss   (3.1,0.015)

    m_x.setVal ( x )
    m_y.setVal ( y )
    m_z.setVal ( z )
    
    dataset.add ( varset  )

## BxBxB
for i in xrange(0,10000) :

    x = random.uniform (  *m_y.minmax() )
    y = random.uniform (  *m_y.minmax() )
    z = random.uniform (  *m_z.minmax() )

    m_x.setVal ( x )
    m_y.setVal ( y )
    m_z.setVal ( z )

    dataset.add ( varset  )


print dataset

import Ostap.FitModels as Models

s1 = Models.Gauss_pdf (  'SX' , xvar = m_x ,
                         ## mean  = ( 3.100 , 3.090 , 3.110 ) ,
                         ## sigma = ( 0.010 , 0.015 , 0.020 ) )
                         mean  = 3.100 ,
                         sigma = 0.015 ) 
s2 = Models.Gauss_pdf (  'SY' , xvar = m_y ,
                         mean  = s1.mean   , 
                         sigma = s1.sigma  )
s3 = Models.Gauss_pdf (  'SZ' , xvar = m_z ,
                         mean  = s1.mean   , 
                         sigma = s1.sigma  )

models = []

model1 = Models.Fit3D (
    signal_1 = s1   ,
    signal_2 = s2   ,
    signal_3 = s3   ,
    bkgX1    = -1   ,
    bkgY1    = -1   ,
    bkgZ1    = -1   ,
    bkgX2    = None ,
    bkgY2    = None ,
    bkgZ2    = None ,
    bkgX3    = None ,
    bkgY3    = None ,
    bkgZ3    = None ,
    )

model1.SSS = 10000
model1.SSB =  5000
model1.SBS =  5000
model1.BSS =  5000
model1.SBB =  8000
model1.BSB =  8000
model1.BBS =  8000
model1.BBS =  8000
model1.BBB = 10000

models.append ( model1 )

with rooSilent() : 
    r1 = model1.fitTo ( dataset )
    r1 = model1.fitTo ( dataset )
    r1 = model1.fitTo ( dataset )

print r1

#
## check that everything is serializeable
# 
import Ostap.ZipShelve   as DBASE
with DBASE.tmpdb() as db :
    db['x,y,vars'] = m_x, m_y,m_z, varset
    db['dataset' ] = dataset
    db['models'  ] = models
    db['result'  ] = r1
    db.ls()
    

# =============================================================================
# The END 
# =============================================================================
