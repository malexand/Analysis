################################################################################
# Package: DecayTreeTupleTracking
################################################################################
gaudi_subdir(DecayTreeTupleTracking)

gaudi_depends_on_subdirs(Tr/TrackFitEvent
                         Phys/DecayTreeTupleBase)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(DecayTreeTupleTracking
                 src/*.cpp
                 LINK_LIBRARIES TrackFitEvent DecayTreeTupleBaseLib)
