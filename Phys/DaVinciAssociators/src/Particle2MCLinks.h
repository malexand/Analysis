#ifndef Particle2MCLinks_H
#define Particle2MCLinks_H 1

// Include files
// from STL
#include <string>


// local
#include "Kernel/Particle2MCLinker.h"
#include "AsctAlgorithm.h"

/** @class Particle2MCLinks Particle2MCLinks.h
 *
 *
 *  @author Philippe Charpentier
 *  @date   10/05/2002
 */
class Particle2MCLinks : public AsctAlgorithm {
public:
  /// Standard constructor
  Particle2MCLinks( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~Particle2MCLinks( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

private:
  Object2MCLinker< LHCb::ProtoParticle >*   m_chargedLink;
  Object2MCLinker< LHCb::ProtoParticle >*   m_neutralLink;
  std::vector<std::string>  m_chargedPPLocation;
  std::vector<std::string>  m_neutralPPLocation;
};
#endif // Particle2MCLinks_H
