#ifndef NEUTRALPP2MC_H
#define NEUTRALPP2MC_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "AsctAlgorithm.h"


/** @class NeutralPP2MC NeutralPP2MC.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2008-09-25
 */
class NeutralPP2MC  : public AsctAlgorithm{
public:
  /** standard execution of the algorithm
   *  @return StatusCode
   */
  StatusCode execute    () override;
  /** Standard constructor
   *  @param name name of the algorithm
   *  @param svc  service locator
   */
  NeutralPP2MC( const std::string& name , ISvcLocator*       svc  );
  StatusCode initialize() override;
private:
  std::string m_mcTable ;
  std::string m_test;
};
#endif // NEUTRALPP2MC_H
