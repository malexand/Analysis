################################################################################
# Package: DaVinciAssociators
################################################################################
gaudi_subdir(DaVinciAssociators)

gaudi_depends_on_subdirs(Calo/CaloUtils
                         Event/LinkerEvent
                         Event/MCEvent
                         Event/PhysEvent
                         Event/TrackEvent
                         Kernel/Relations
                         Phys/DaVinciKernel
                         Phys/DaVinciMCKernel)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(DaVinciAssociators
                 src/*.cpp
                 INCLUDE_DIRS AIDA
                 LINK_LIBRARIES CaloUtils LinkerEvent MCEvent PhysEvent TrackEvent RelationsLib DaVinciKernelLib DaVinciMCKernelLib)

