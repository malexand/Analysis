################################################################################
# Package: DecayTreeTupleMuonCalib
################################################################################
gaudi_subdir(DecayTreeTupleMuonCalib)

gaudi_depends_on_subdirs(Det/MuonDet
                         Phys/DecayTreeTupleBase)

find_package(HepMC)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(DecayTreeTupleMuonCalib
                 src/*.cpp
                 INCLUDE_DIRS HepMC
                 LINK_LIBRARIES HepMC MuonDetLib DecayTreeTupleBaseLib)

