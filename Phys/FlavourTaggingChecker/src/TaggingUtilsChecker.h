#ifndef USER_TAGGINGUTILSCHECKER_H
#define USER_TAGGINGUTILSCHECKER_H 1

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
// from Event
#include "MCInterfaces/IPrintMCDecayTreeTool.h"
#include "ITaggingUtilsChecker.h"
#include <Kernel/IDistanceCalculator.h>
#include <Kernel/IDVAlgorithm.h>
#include <Kernel/GetIDVAlgorithm.h>

/** @class TaggingUtilsChecker TaggingUtilsChecker.h
 *
 *  Utility Tool
 *
 *  @author Marco Musy
 *  @date   09/06/2007
 */

class TaggingUtilsChecker : public GaudiTool,
                            virtual public ITaggingUtilsChecker {

public:
  /// Standard constructor
  TaggingUtilsChecker( const std::string& type,
		const std::string& name,
		const IInterface* parent );
  ~TaggingUtilsChecker( ); ///< Destructor
  StatusCode initialize() override;    ///<  initialization
  StatusCode finalize  () override;    ///<  finalization

  //-------------------------------------------------------------
  StatusCode calcIP( const LHCb::Particle* ,
		     const LHCb::VertexBase* , double&, double&) override;
  StatusCode calcIP( const LHCb::Particle*,
		     const LHCb::RecVertex::ConstVector& ,
		     double& , double& ) override;
  StatusCode calcIPPU( const LHCb::Particle*,
                       const LHCb::Particle*,
                       const LHCb::RecVertex::ConstVector& ,
                       const double ,
                       double& , double&, double&,
                       double& , double&, double&,
                       double& , double&, double&,
                       int& , double&, double&) override;
  StatusCode calcDOCAmin( const LHCb::Particle* ,
			  const LHCb::Particle* ,
			  const LHCb::Particle* , double& , double& ) override;
  int countTracks( LHCb::Particle::ConstVector& ) override;
  const LHCb::Particle* motherof( const LHCb::Particle* ,
                                  const LHCb::Particle::ConstVector& ) override;
  bool isinTree(const LHCb::Particle*, LHCb::Particle::ConstVector&, double&) override;
  LHCb::Particle::ConstVector FindDaughters( const LHCb::Particle* ) override;
  const LHCb::MCParticle* originof( const LHCb::MCParticle*  ) override;
  int comes_from_excitedB(const LHCb::MCParticle* , const LHCb::MCParticle* ) override;

  //-------------------------------------------------------------

private:
  //  no more valid for Sim08 (no HepMC classes)
  //  LHCb::MCParticle::ConstVector prodsBstar( const LHCb::MCParticle*  );
  //  LHCb::MCParticle::ConstVector prodsBstarstar( const LHCb::MCParticle*  );
  //  LHCb::MCParticle::ConstVector prodsBstring( const LHCb::MCParticle*  );
  //  HepMC::GenParticle* HEPassociated(const LHCb::MCParticle* );
  //  LHCb::MCParticle* associatedofHEP(HepMC::GenParticle* );

  IPrintMCDecayTreeTool* m_debug;
  const IDistanceCalculator *m_Dist;
  IDVAlgorithm* m_dva;

};

//===============================================================//
#endif // USER_TAGGINGUTILSCHECKER_H
