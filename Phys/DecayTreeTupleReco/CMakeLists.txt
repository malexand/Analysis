################################################################################
# Package: DecayTreeTupleReco
################################################################################
gaudi_subdir(DecayTreeTupleReco)

gaudi_depends_on_subdirs(Phys/DecayTreeTupleBase
                         Hlt/HltDAQ
                         Rich/RichUtils
                         Event/HltEvent
  )

find_package(Boost)
find_package(ROOT)
find_package(Vc)

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS}
                           ${Vc_INCLUDE_DIR})

gaudi_add_module(DecayTreeTupleReco
                 src/*.cpp
                 LINK_LIBRARIES DecayTreeTupleBaseLib HltEvent RichUtils)
