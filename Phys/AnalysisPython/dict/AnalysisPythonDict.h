// ============================================================================
#ifndef DICT_ANALYSISPYTHONDICT_H
#define DICT_ANALYSISPYTHONDICT_H 1
// ============================================================================
// Include files
// ============================================================================
// AnalysiPython
// ============================================================================
#if defined(__CLING__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winconsistent-missing-override"
#endif

#include "Analysis/PySelector.h"
#include "Analysis/PySelectorWithCuts.h"
#include "Analysis/RooMakeHistos.h"
#include "Analysis/UStat.h"
#include "Analysis/HStats.h"
#include "Analysis/SFactor.h"
#include "Analysis/Mute.h"
#include "Analysis/Tee.h"
#include "Analysis/Formula.h"
#include "Analysis/Iterator.h"
#include "Analysis/PyIterator.h"
#include "Analysis/StatVar.h"
#include "Analysis/HProject.h"
#include "Analysis/ErrorHandlers.h"
#include "Analysis/Printable.h"
#include "Analysis/Tmva.h"
// ============================================================================
#include "Analysis/Models.h"
#include "Analysis/Models2D.h"
#include "Analysis/Models3D.h"
// ===========================================================================
#include "Analysis/IFuncs.h"
#include "Analysis/Funcs.h"
#include "Analysis/PyFuncs.h"
#include "Analysis/Notifier.h"
// ===========================================================================
#include "RooFormulaVar.h"
// ===========================================================================
#if defined(__CLING__)
#pragma clang diagnostic pop
#endif

// ============================================================================
/** @file
 *
 *  Helper file to build Reflex dictionary for AnalsyisPythonPackage
 *
 *  @author Vanya Belyaev Ivan.Belyaev@cern.ch
 *  @date   2011-01-21
 *
 */
// ============================================================================
// The END
// ============================================================================
#endif // DICT_ANALYSISPYTHONDICT_H
// ============================================================================
