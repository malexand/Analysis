// ============================================================================
// Include files
// ============================================================================
// local
// ============================================================================
#include "Analysis/IFuncs.h"
// ============================================================================
/** @file 
 *  Implementation file for classes
 *  @date 2018-03-31 
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 */
// ============================================================================
// desructor
// ============================================================================
Analysis::IFuncTree::~IFuncTree(){}
// ============================================================================
// desructor
// ============================================================================
Analysis::IFuncData::~IFuncData (){}
// ============================================================================
// The END 
// ============================================================================

