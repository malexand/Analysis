#ifndef MCPARTICLEFLOW_H
#define MCPARTICLEFLOW_H 1

// STL.
#include <unordered_set>
#include <unordered_map>

// Gaudi.
#include "GaudiAlg/GaudiHistoAlg.h"

// Event.
#include "Event/MCParticle.h"

using namespace std;
using namespace LHCb;

/**
 * Particle flow (PF) algorithm class for use with MCParticles.
 *
 * PF takes inputs and creates a common output of MCParticles,
 * ensuring only stable MCParticles from collisions (not material
 * interactions) with no double counting; if a particle is designated
 * as stable, its daughters are not included in the output.
 *
 * The PF algorithm itself does not filter input; the passed inputs
 * must already be filtered. Two classes of inputs can be passed with
 * class names 'MCParticle' and 'PID'. Inputs are set via the 'Inputs'
 * property which must be of the form [[<class name>, <object type>,
 * <location>], ...] where all three arguments for each input are
 * strings. The object type can be 'ban', 'particle', or
 * 'daughters'. If 'ban', the MCParticle and all its daughters are
 * banned from the PF output. If 'particle', the MCParticle is treated
 * as stable and its daughters are not written. Finally, if
 * 'daughters', only final state daughters are written.
 *
 * For inputs of class 'PID' the provided location should be a string
 * of comma separated PDG IDs, e.g. '12,-12,14,-14,16,-16'. The 'ban'
 * or 'particle' rules from a 'PID' input apply to all 'MCParticle'
 * inputs that follow. For example, if the first input is ['PID',
 * 'ban', '12,-12,14,-14,16,-16'], then neutrinos will be banned from
 * all remaining inputs. The 'PID' rules are cumulative. Note that a
 * 'PID' input of object type 'daughters' has no effect.
 *
 * @class  McParticleFlow
 * @file   McParticleFlow.h
 * @author Cedric Potterat and Philip Ilten
 * @date   2016-05-25
 */
class McParticleFlow : public GaudiHistoAlg {
public:
  /// Constructor.
  McParticleFlow(const string &name, ISvcLocator *svc);
  StatusCode initialize() override; ///< Initialize.
  StatusCode execute   () override; ///< Execute.

private:
  /**
   * Input structure class for PF. 
   *
   * Used to parse the input lists passed via #m_inLocs.
   */
  class Input {
  public:
    /// Constructor.
    Input(const unsigned int *idx = 0, const vector<string> *prp = 0,
	  unordered_set<int> *idb = 0, unordered_set<int> *idp = 0);
    /// Print the input configuration to #out.
    inline void print(MsgStream &msg, bool idx = true);
    
    // Members.
    string warn;  ///< Any warnings that may have been generated.
    string name;  ///< The class name of objects from this input.
    string type;  ///< The object type for this input.
    string loc;   ///< The TES location for this input.
    bool   valid; ///< Flag if input is valid.
    bool   ban;   ///< Flag if objects from this input should be banned.
    bool   prt;   ///< Flag if MCParticle should be written as a particle.
    unsigned int index;         ///< Index for this input.
    unordered_set<int> pidsBan; ///< Set of PIDs to ban.
    unordered_set<int> pidsPrt; ///< Set of PIDs to treat as particles.
  };

  // Additional methods.
  /// Add an MCParticle and its daughters to the PF output MCParticles.
  void add(const MCParticle *prt, const Input &in, int key);
  /**
   * Recursively determine the status of an MCParticle decay tree.
   *
   * #used is set to true if any particle in the decay tree has been
   * used. #prts are all particles from the decay tree (including the
   * mother) with a flag whether the particle should be saved.
   */
  void status(const MCParticle *mom, const Input &in, 
	      vector<pair<const MCParticle*, bool> > &prts, 
	      bool &stable, bool &banned, bool &used);
  /// Check if a particle is from a collision.
  bool primary(const MCParticle *prt);

  // Input/output property members.
  /// PF inputs of the form [<class name>, <object type>, <location>].
  vector< vector<string> > m_inLocs;
  /// PF output Particle location.
  string m_outLoc;

  // Additional members (not properties).
  MCParticles                     *m_prts;   ///< Output MCParticle container.
  vector<Input>                    m_inPrts; ///< Vector of MCParticle inputs.
  unordered_set<const MCParticle*> m_used;   ///< Set of used MCParticles.
  unordered_set<const MCParticle*> m_save;   ///< Set of MCParticles to save.
};

#endif // MCPARTICLEFLOW_H
