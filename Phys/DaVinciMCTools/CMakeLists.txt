################################################################################
# Package: DaVinciMCTools
################################################################################
gaudi_subdir(DaVinciMCTools)

gaudi_depends_on_subdirs(Calo/CaloInterfaces
                         Calo/CaloUtils
                         Event/GenEvent
                         Event/LinkerEvent
                         Event/MCEvent
                         Event/PhysEvent
                         Event/RecEvent
                         Kernel/MCInterfaces
                         Phys/DaVinciKernel
                         Phys/DaVinciMCKernel
                         Phys/LoKiPhysMC)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(DaVinciMCTools
                 src/*.cpp
                 INCLUDE_DIRS Kernel/MCInterfaces
                 LINK_LIBRARIES CaloUtils GenEvent LinkerEvent MCEvent PhysEvent RecEvent DaVinciKernelLib DaVinciMCKernelLib LoKiPhysMCLib)

